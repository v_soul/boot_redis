package com.redis.demoredis;

import com.redis.demoredis.config.RedisService;
import com.redis.demoredis.domain.Data;
import com.redis.demoredis.domain.PersonModel;
import com.redis.demoredis.domain.UserVo;
import com.redis.demoredis.util.RedisKeyUtil;
import jdk.management.resource.ResourceId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.*;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.DoubleSupplier;
import java.util.function.IntSupplier;
import java.util.function.LongSupplier;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Java8StreamApplicationTests {

    @Test
    public void java8Lambda() {

        /**
         * <P>不需要参数返回值为5, 数字自动返回类型为int doubble long不同类型.get不同类型获取，
         * 例如 IntSupplier LongSupplier doubbleSupplier</P>
         */
        IntSupplier supplier = () -> 5;
        System.out.println(supplier.getAsInt());

    }

    @Test
    public void list() {

        List<PersonModel> data = Data.getData();
        data.forEach(System.out::println);

        Comparator<Integer> c2 = (x, y) -> Integer.compare(x, y);
        Comparator<Integer> c1 = Integer::compare;
//        data.forEach(p -> System.out.println(p.getAge()));
//
//        data.stream().filter(p -> p.getAge() >= 20).forEach(t -> t.setName("liqnigeng"));
//        System.out.println(data);
//        List<PersonModel> collect = data.stream().filter(p -> p.getAge() >= 20).collect(toList());
//        collect.forEach(p -> p.setName("liqingdfadfasd"));
//        System.out.println(collect);


//        List<PersonModel> temp = data.stream().filter(p -> "男".equals(p.getSex())).collect(toList());
//
//        List<PersonModel> temp2 = data.stream().filter(p -> ("男".equals(p.getSex()) && 20 > p.getAge())).collect(toList());
//
//        System.out.println(temp2);
//
//        Map<String, PersonModel> collect = data.stream().collect(Collectors.toMap(PersonModel::getName, personModel -> personModel));
//
//
//
//        List<String> ci = data
//                .stream()
//                .map(personModel -> personModel.getName())
//                .collect(toList());
//
//        List<Integer> age = data
//                .stream()
//                .map(PersonModel::getAge)
//                .collect(toList());
//
//        System.out.println(ci);
    }

    @Test
    public void map() {

        List<PersonModel> data = Data.getData();
        //返回类型不一样
        List<String> collect = data.stream()
                .flatMap(person -> Arrays.stream(person.getName().split(" "))).collect(toList());

        System.out.println(collect);

        List<Stream<String>> collect1 = data.stream()
                .map(person -> Arrays.stream(person.getName().split(" "))).collect(toList());
        System.out.println(collect1);
        //用map实现
        List<String> collect2 = data.stream()
                .map(person -> person.getName().split(" "))
                .flatMap(Arrays::stream).collect(toList());
        //另一种方式
        List<String> collect3 = data.stream()
                .map(person -> person.getName().split(" "))
                .flatMap(str -> Arrays.asList(str).stream()).collect(toList());
        System.out.println(collect3);
    }


    @Test
    public void reduceTest() {
        //累加，初始化值是 10
        Integer reduce = Stream.of(1, 2, 3, 4)
                .reduce(10, (count, item) -> {
                    System.out.println("count:" + count);
                    System.out.println("item:" + item);
                    return count + item;
                });
        System.out.println(reduce);

        Integer reduce1 = Stream.of(1, 2, 3, 4)
                .reduce(0, (x, y) -> x + y);
        System.out.println(reduce1);

        String reduce2 = Stream.of("1", "2", "3")
                .reduce("0", (x, y) -> (x + "," + y));
        System.out.println(reduce2);
    }


    /**
     * toList
     */
    @Test
    public void toListTest() {
        List<PersonModel> data = Data.getData();
        List<String> collect = data.stream()
                .map(PersonModel::getName)
                .collect(Collectors.toList());

        System.out.println(collect);
    }

    /**
     * toSet
     */
    @Test
    public void toSetTest() {
        List<PersonModel> data = Data.getData();
        Set<String> collect = data.stream()
                .map(PersonModel::getName)
                .collect(Collectors.toSet());

        System.out.println(collect);
    }

    /**
     * toMap
     */
    @Test
    public void toMapTest() {
        List<PersonModel> data = Data.getData();
        Map<String, Integer> collect = data.stream()
                .collect(
                        Collectors.toMap(PersonModel::getName, PersonModel::getAge)
                );

        data.stream()
                .collect(Collectors.toMap(per -> per.getName(), value -> {
                    return value + "1";
                }));
    }

    /**
     * 指定类型
     */
    @Test
    public void toTreeSetTest() {
        List<PersonModel> data = Data.getData();
        TreeSet<PersonModel> collect = data.stream()
                .collect(Collectors.toCollection(TreeSet::new));
        System.out.println(collect);
    }

    /**
     * 分组
     */
    @Test
    public void toGroupTest() {
        List<PersonModel> data = Data.getData();
        Map<Boolean, List<PersonModel>> collect = data.stream()
                .collect(Collectors.groupingBy(per -> "男".equals(per.getSex())));
        System.out.println(collect);
    }

    /**
     * 分隔
     */
    @Test
    public void toJoiningTest() {
        List<PersonModel> data = Data.getData();
        String collect = data.stream()
                .map(personModel -> personModel.getName())
                .collect(Collectors.joining(",", "{", "}"));
        System.out.println(collect);
    }

    /**
     * 自定义
     */
    @Test
    public void reduce() {
        List<String> collect = Stream.of("1", "2", "3").collect(
                Collectors.reducing(new ArrayList<String>(), x -> Arrays.asList(x), (y, z) -> {
                    y.addAll(z);
                    return y;
                }));
        System.out.println(collect);
    }

    @Test
    public void distince () {
        String a = "1,2,3,4,5,6,7,8,9,22,11,2,44,556,234,234,21,122,3,3,12,123,3,25,453,63,433,433,3,4";
        List<String> strings = Arrays.asList(a.split(","));

        List<Integer> collect = strings.stream()
                .map(Integer::new)
                .filter(p -> prime(p))
                .distinct().collect(toList());
        Integer reduce = collect.stream().reduce(0, (x, y) -> x + y);
//                .reduce(0,(x,y) -> x+y);

        System.out.println(collect);
        System.out.println(reduce);

    }

    public boolean prime(int n) {
        if (n < 2) {
            return false;
        }
        if (n == 2 || n == 3) {
            return true;
        } else {
            int a = (int) Math.sqrt(n);
            for (int i = 2; i <= a; i++) {
                if (n % i == 0) {
                    return false;
                }
            }
            return true;
        }
    }



}
